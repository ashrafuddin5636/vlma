/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.exception;

public class InvalidServerNameException extends RuntimeException {

    private static final long serialVersionUID = -3745442251744729677L;

    public InvalidServerNameException() {
        super();
    }

    public InvalidServerNameException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }

    public InvalidServerNameException(String arg0) {
        super(arg0);
    }

    public InvalidServerNameException(Throwable arg0) {
        super(arg0);
    }

    @Override
    public String toString() {
        return "This server name is not valid";
    }

}
