/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * A media.
 *
 * @author Sylvain Cadilhac <sylv at videolan.org>
 */
public abstract class Media implements Serializable {

    private static final long serialVersionUID = 288197921243395310L;

    private Integer id;

    protected String name;

    protected List<Program> programs;

    public Media() {
        programs = new ArrayList<Program>();
    }

    /**
     * Gets the Media ID.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the media ID.
     *
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gives the program associated with the media.
     *
     * @return the program
     */
    public List<Program> getPrograms() {
        return programs;
    }

    public void setPrograms(List<Program> programs) {
        this.programs = programs;
    }

    /**
     * Add a program to this media.
     *
     * @param program the program to add
     */
    public void addProgram(Program program) {
        this.programs.add(program);
    }

    /**
     * Remove a program.
     *
     * @param program the program to remove
     */
    public void removeProgram(Program program) {
        this.programs.remove(program);
    }

    /**
     * Sets the media name.
     *
     * @param name the media name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gives the media name.
     *
     * @return the media name
     */
    public String getName() {
        return name;
    }

}
