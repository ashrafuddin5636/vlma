/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma;

import java.io.IOException;
import java.net.URL;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import org.videolan.vlma.exception.AlreadyExistsException;
import org.videolan.vlma.exception.NotFoundException;
import org.videolan.vlma.model.Command;
import org.videolan.vlma.model.Media;
import org.videolan.vlma.model.Program;
import org.videolan.vlma.model.Satellite;
import org.videolan.vlma.model.Server;

/**
 * This interface exposes some business logic of VLMa to be called remotely
 * through RMI.
 *
 * @author Sylvain Cadilhac <sylv at videolan.org>
 * @author Adrien Grand <jpountz at videolan.org>
 */
public interface Data extends Remote {

    /*************************************************************************
     *
     * Interaction with VLMa daemon
     *
     *************************************************************************/

    /**
     * Reload VLMa configuration and data.
     *
     * @throws RemoteException
     */
    void reload() throws RemoteException;

    /**
     * Stop VLMa.
     *
     * @throws RemoteException
     */
    void stop() throws RemoteException;

    /*************************************************************************
     *
     * Management of VLMa configuration
     *
     *************************************************************************/

    /**
     * Gets the property associated with a given key.
     *
     * @param key the property key
     * @return the property value
     * @throws RemoteException
     */
    String getString(String key) throws RemoteException;

    /**
     * Gets the property associated with a given key.
     *
     * @param key the property key
     * @return the property value
     * @throws RemoteException
     */
    Integer getInt(String key) throws RemoteException;

    /**
     * Gets the property associated with a given key.
     *
     * @param key the property key
     * @return the property value
     * @throws RemoteException
     */
    List<String> getList(String key) throws RemoteException;

    /**
     * Sets the property value associated with a given key.
     *
     * @param key the property key
     * @param value the property value to set
     * @throws RemoteException
     */
    void setProperty(String key, Object value) throws RemoteException;

    /**
     * Removes the provided key from the configuration.
     *
     * @param key the property key
     * @throws RemoteException
     */
    void clearProperty(String key) throws RemoteException;

    /*************************************************************************
     *
     * Data Access Objects
     *
     *************************************************************************/

    /**
     * Returns the list of the satellites known by the data interface.
     *
     * @see Satellite
     * @return la liste des satellites
     * @throws RemoteException
     */
    List<Satellite> getSatellites() throws RemoteException;

    /**
     * Returns a satellite according to its ID.
     *
     * @param satellite the ID of the satellite to retrieve
     * @return the satellite
     * @throws SatelliteDoesNotExistException No satellite has the specified ID.
     * @throws RemoteException
     */
    Satellite getSatellite(int satellite) throws NotFoundException, RemoteException;

    /**
     * Adds a satellite.
     *
     * @param satellite the satellite to add
     * @throws SatelliteAlreadyExistsException
     *             Another satellite already has this name.
     * @throws RemoteException
     */
    void add(Satellite satellite) throws AlreadyExistsException, RemoteException;

    /**
     * Synchronize the database with the provided satellite.
     *
     * @param satellite the satellite to update
     * @throws SatelliteAlreadyExistsException
     *             The provided satellite is not in the database.
     * @throws RemoteException
     */
    void update(Satellite satellite) throws NotFoundException, RemoteException;

    /**
     * Deletes a satellite.
     *
     * @param satellite the satellite ID
     * @throws NotFoundException
     *             No satellite has the specified ID.
     * @throws RemoteException
     */
    void remove(Satellite satellite) throws NotFoundException, RemoteException;

    /**
     * Returns the collection of available satellites.
     *
     * @see Server
     * @return the server list
     * @throws RemoteException
     */
    List<Server> getServers() throws RemoteException;

    /**
     * Returns a satellite according to its ID.
     *
     * @see Server
     * @see Server#getId()
     * @param server the server ID
     * @return the server
     * @throws ServerDoesNotExistException
     *             No server has the specified ID.
     * @throws RemoteException
     */
    Server getServer(int server) throws NotFoundException, RemoteException;

    /**
     * Create a server.
     *
     * @see Server
     * @param server the server to add to the database
     * @throws ServerAlreadyExistsException
     *             Another server has already that name.
     * @throws RemoteException
     */
    void add(Server server) throws AlreadyExistsException, RemoteException;

    /**
     * Updates a server.
     *
     * @param server the server to update
     * @throws NotFoundException
     *             The satellite is not in the database.
     * @throws RemoteException
     */
    void update(Server server) throws NotFoundException, RemoteException;

    /**
     * Deletes a server.
     *
     * @param server the server
     * @throws NotFoundException
     *             No server has the specified ID.
     * @throws RemoteException
     */
    void remove(Server server) throws NotFoundException, RemoteException;

    /**
     * Returns available medias.
     *
     * @return the media list
     * @throws RemoteException
     */
    List<Media> getMedias() throws RemoteException;

    /**
     * Returns a media according to its ID.
     *
     * @param media the media ID
     * @return the media
     * @throws RemoteException
     */
    Media getMedia(int media) throws RemoteException;

    /**
     * Adds a media to the list of handled media.
     *
     * @param media the media to add
     * @throws RemoteException
     */
    void add(Media media) throws RemoteException;

    /**
     * Updates a media.
     *
     * @param media the media to update
     * @throws MediaDoesNotExistException
     *             The provided media is not in the database.
     * @throws RemoteException
     */
    void update(Media media) throws NotFoundException, RemoteException;

    /**
     * Deletes a media
     *
     * @param media the media ID
     * @throws NotFOundException
     *             No media has the specified ID.
     * @throws RemoteException
     */
    void remove(Media media) throws NotFoundException, RemoteException;

    /*************************************************************************
     *
     * Various utilities
     *
     *************************************************************************/

    /**
     * Creates a program which will have the default streaming strategy
     * provided by the user.
     *
     * @return the program
     * @throws RemoteException
     */
    Program newProgram() throws RemoteException;

    /**
     * Gets the media URL.
     *
     * @param media
     * @return the media URL
     * @throws NotProgrammedException this media is not programmed
     * @throws RemoteException
     */
    String getUrl(Program program) throws RemoteException;

    /**
     * Updates the list of a satellite channels using an URL like
     * http://www.satcodx2.com/_data/0192.txt.
     *
     * @param source
     *            the url to use to update
     * @throws IOException
     *             Impossible to fetch the channel list.
     * @throws RemoteException
     */
    void updateSatChannels(URL source) throws IOException, RemoteException;

    /**
     * Executes orders.
     *
     * @throws RemoteException
     */
    void giveOrders() throws RemoteException;

    /**
     * Gets the commands sent to VLM instances through telnet.
     *
     * @return the list of commands sent to servers
     * @throws RemoteException
     */
    List<Command> getCommands() throws RemoteException;

    /**
     * Checks if all the VLC of the servers are up.
     *
     * @throws RemoteException
     */
    void startCheckAllVLCs() throws RemoteException;

    /**
     * Retrieve logs from VLC.
     *
     * @param server
     * @return VLC logs
     */
    List<String> getVlcLogTail(Server server) throws RemoteException, IOException;

    /**
     * Get the version of the running VLC.
     *
     * @param server the server VLC runs on
     * @return VLC version
     */
    String getVlcVersion(Server server) throws RemoteException, IOException;

    /**
     * Restart VLC on the provided server.
     *
     * @param server the server VLC runs on
     */
    void restartVlc(Server server) throws RemoteException, IOException;

    /**
     * Get VLC uptime
     *
     * @param server the server VLC runs on
     * @return VLC uptime
     */
    Long getVlcUptime(Server server) throws RemoteException, IOException;

    /**
     * Run the provided command in VLC telnet interface and returns the response.
     */
    String getVlcResponse(Server server, String command) throws RemoteException, IOException;

}
