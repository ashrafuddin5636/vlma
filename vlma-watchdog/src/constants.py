#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2009-2010 Adrien Grand
#
# This file is part of VLMa.
#
# VLMa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# VLMa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with VLMa. If not, see <http://www.gnu.org/licenses/>.


class Enum:

  def __init__(self, name):
    self.name = name

  def __str__(self):
    return "%s.%s" %(self.__class__, self.name)

  def __repr__(self):
    return "<%s: %s>" %(self.__class__, self.name)


class RestartTrigger(Enum):
  def __init__(self, name):
    Enum.__init__(self, name)

RestartTrigger.CPU_LOAD = RestartTrigger("CPU_LOAD")
RestartTrigger.STREAMER_CPU = RestartTrigger("STREAMER_CPU")
RestartTrigger.STREAMER_MEMORY = RestartTrigger("STREAMER_MEMORY")

class Logging(Enum):
  def __init__(self, name):
    Enum.__init__(self, name)

Logging.DISPLAY_STDOUT_TO = Logging("DISPLAY_STDOUT_TO")
Logging.DISPLAY_STDERR_TO = Logging("DISPLAY_STDERR_TO")
Logging.TAIL_LENGTH = Logging("TAIL_LENGTH")
