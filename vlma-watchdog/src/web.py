#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2009-2010 Adrien Grand
#
# This file is part of VLMa.
#
# VLMa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# VLMa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with VLMa. If not, see <http://www.gnu.org/licenses/>.


import conf, constants, logging, threading, serialization, sys, utils, validation
from streamer.api import *
try:
  from twisted.web import server, resource, static
  from twisted.internet import reactor
  from twisted.web import http
except ImportError:
  print "Twisted is missing, this program won't run."
  print "Install Twisted first."
  sys.exit(1)

class Error:

  def __init__(self, code, msg):
    self.code = code
    self.msg = msg


class ErrorCode(constants.Enum):

  def __init__(self, http_status, name):
    constants.Enum.__init__(self, name)
    self.http_status = http_status

ErrorCode.BAD_REQUEST        = ErrorCode(http.BAD_REQUEST, "BAD_REQUEST")
ErrorCode.METHOD_NOT_ALLOWED = ErrorCode(http.NOT_ALLOWED, "METHOD_NOT_ALLOWED")
ErrorCode.UNAUTHORIZED       = ErrorCode(http.UNAUTHORIZED, "UNAUTHORIZED")
ErrorCode.STREAMER_FAILED    = ErrorCode(http.INTERNAL_SERVER_ERROR, "STREAMER_FAILED")
ErrorCode.NO_FREE_ADAPTER    = ErrorCode(http.INTERNAL_SERVER_ERROR, "NO_FREE_ADAPTER")

class StringBuffer:

  def __init__(self):
    self.data = []

  def write(self, s):
    self.data.append(s)

  def __str__(self):
    return "".join(self.data)


class Server(threading.Thread):

  def __init__(self, streamers):
    self.__logger = logging.getLogger("watchdog.web")
    threading.Thread.__init__(self)
    self.__streamers = streamers
    self.setDaemon(True)

  def run(self):
    root = resource.Resource()
    root.putChild("", IndexResource(self.__streamers))
    root.putChild("schema.xsd", static.File("schema.xsd", defaultType='text/xml'))
    order = OrdersResource(self.__streamers)
    root.putChild("order", order)
    order.putChild("add", OrderAddResource(self.__streamers))
    order.putChild("remove", OrderRemoveResource(self.__streamers))
    system = resource.Resource()
    root.putChild("system", system)
    system.putChild("metric", SystemMetricsResource())
    streamers = StreamersResource(self.__streamers)
    root.putChild("streamer", streamers)
    for streamer in self.__streamers:
      s = StreamerResource(streamer)
      streamers.putChild(streamer.id, s)
      s.putChild("log", StreamerLogsResource(streamer))
      s.putChild("metric", StreamerMetricsResource(streamer))
      s.putChild("order", StreamerOrdersResource(streamer))
      s.putChild("restart", StreamerRestartResource(streamer))
    site = server.Site(root)
    reactor.listenTCP(conf.SERVER_PORT, site)
    self.__logger.info("Starting web server on http://localhost:%d", conf.SERVER_PORT)
    reactor.run(installSignalHandlers=False)


class AuthenticationRequiredResource(resource.Resource):

  def __init__(self):
    resource.Resource.__init__(self)

  def isAuthenticated(self, request):
    if(request.getUser() != conf.SERVER_LOGIN or request.getPassword() != conf.SERVER_PASSWORD):
      error = Error(ErrorCode.UNAUTHORIZED, "Authentication failed")
      request.setResponseCode(error.code.http_status)
      request.setHeader('WWW-authenticate', 'basic realm="Watchdog HTTP interface"')
      serialization.error_to_xml(error, request)
      return False
    return True

  def render_GET_authenticated(self, request):
    error = Error(ErrorCode.METHOD_NOT_ALLOWED, "The GET method is not supported by this resource")
    request.setResponseCode(error.code.http_status)
    prepare_xml_response(request)
    serialization.error_to_xml(error)

  def render_GET_not_authenticated(self, request):
    return ""

  def render_GET(self, request):
    if not self.isAuthenticated(request):
      result = self.render_GET_not_authenticated(request)
    else:
      result = self.render_GET_authenticated(request)
    if result is None:
      return ""
    else:
      return str(result)

  def render_POST_authenticated(self, request):
    error = Error(ErrorCode.METHOD_NOT_ALLOWED, "The POST method is not supported by this resource")
    request.setResponseCode(error.code.http_status)
    prepare_xml_response(request)
    serialization.error_to_xml(error)

  def render_POST_not_authenticated(self, request):
    return ""

  def render_POST(self, request):
    if not self.isAuthenticated(request):
      result = self.render_POST_not_authenticated(request)
    else:
      result = self.render_POST_authenticated(request)
    if result is None:
      return ""
    else:
      return str(result)


def prepare_xml_response(request):
  request.setHeader('content-type', 'text/xml')
  request.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")


class IndexResource(AuthenticationRequiredResource):

  def __init__(self, streamers):
    AuthenticationRequiredResource.__init__(self)
    self.links = [
      ("/order", "Orders"),
      ("/streamer", "Streamers"),
      ("/system/metric", "System metrics")
    ]
    for streamer in streamers:
     self.links.append(("/streamer/%s" %streamer.id, "Streamer %s (%s)" %(streamer.id, streamer.type)))
     self.links.append(("/streamer/%s/log" %streamer.id, "Streamer %s's logs" %streamer.id))
     self.links.append(("/streamer/%s/metric" %streamer.id, "Streamer %s's metrics" %streamer.id))
     self.links.append(("/streamer/%s/order" %streamer.id, "Streamer %s's orders" %streamer.id))

  def render_GET_authenticated(self, request):
    for link in self.links:
      request.write("<a href=\"%s\">%s</a><br>" %(link[0], link[1]))


class OrdersResource(AuthenticationRequiredResource):

  def __init__(self, streamers):
    AuthenticationRequiredResource.__init__(self)
    self.streamers = streamers

  def render_GET_authenticated(self, request):
    buf = StringBuffer()
    prepare_xml_response(request)
    buf.write("<orders>")
    for streamer in self.streamers:
      for order in streamer.orders.values():
        serialization.order_to_xml(order, buf)
    buf.write("</orders>")
    request.write(str(buf))


class OrderAddResource(AuthenticationRequiredResource):

  def __init__(self, streamers):
    AuthenticationRequiredResource.__init__(self)
    self.streamers = streamers

  def render_POST_authenticated(self, request):
    request.content.seek(0)
    xml = "".join(request.content.readlines())
    request.setHeader('Content-type', 'text/xml')
    buf = StringBuffer()
    buf.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")

    # Parse the incoming XML
    try:
      order = serialization.order_from_xml(xml)
    except serialization.SerializationException, e:
      error = Error(ErrorCode.BAD_REQUEST, "Mal formated request: %s" %str(e))
      request.setResponseCode(error.code.http_status)
      serialization.error_to_xml(error, buf)
      return str(buf)

    # Validate the resulting object
    try:
      validation.validate_order(order)
    except validation.ValidationException, e:
      error = Error(ErrorCode.BAD_REQUEST, "Request not valid: %s" %str(e))
      request.setResponseCode(error.code.http_status)
      serialization.error_to_xml(error, buf)
      return str(buf)

    for streamer in self.streamers:
      if streamer.orders.has_key(order.id):
        try:
          streamer.stop_streaming(streamer.orders[order.id])
        except BaseException, e:
          # TODO: what to do?
          pass

    if isinstance(order, DVBOrder) and utils.is_busy(order.adapter):
      error = Error(ErrorCode.NO_FREE_ADAPTER, "Adapter %d is not available" %order.adapter)
      request.setResponseCode(error.code.http_status)
      serialization.error_to_xml(error, buf)
      return str(buf)

    for streamer in self.streamers:
      if streamer.can_stream(order):
        order.streamer = streamer.id
        try:
          streamer.start_streaming(order)
        except Exception, e:
          error = Error(ErrorCode.STREAMER_FAILED, "Streamer %s could not stream the order: %s" %(str(streamer.id), str(e)))
          request.setResponseCode(error.code.http_status)
          serialization.error_to_xml(error, buf)
          return str(buf)
        streamer.orders[order.id] = order
        serialization.order_to_xml(order, buf)
        return str(buf)


class OrderRemoveResource(AuthenticationRequiredResource):

  def __init__(self, streamers):
    AuthenticationRequiredResource.__init__(self)
    self.streamers = streamers

  def render_POST_authenticated(self, request):
    prepare_xml_response(request)
    order_ids = request.args["id"]
    if len(order_ids) == 0:
      error = Error(ErrorCode.BAD_REQUEST, "Missing parameter'id'")
      request.setResponseCode(error.code.http_status)
      serialization.error_to_xml(error, request)
      return
    buf = StringBuffer()
    buf.write("<orders>")
    for streamer in self.streamers:
      for order_id in order_ids:
        if streamer.orders.has_key(order_id):
          order = streamer.orders[order_id]
          serialization.order_to_xml(order, buf)
          del streamer.orders[order_id]
          try:
            streamer.stop_streaming(order)
          except BaseException, e:
            # TODO: what to do?
            pass
    buf.write("</orders>")
    return str(buf)

class StreamersResource(AuthenticationRequiredResource):

  def __init__(self, streamers):
    AuthenticationRequiredResource.__init__(self)
    self.streamers = streamers

  def render_GET_authenticated(self, request):
    prepare_xml_response(request)
    request.write('<streamers>')
    for streamer in self.streamers:
      serialization.streamer_to_xml(streamer, request)
    request.write('</streamers>')


class StreamerResource(AuthenticationRequiredResource):

  def __init__(self, streamer):
    AuthenticationRequiredResource.__init__(self)
    self.streamer = streamer

  def render_GET_authenticated(self, request):
    prepare_xml_response(request)
    serialization.streamer_to_xml(self.streamer, request)


class StreamerMetricsResource(AuthenticationRequiredResource):

  def __init__(self, streamer):
    AuthenticationRequiredResource.__init__(self)
    self.streamer = streamer

  def render_GET_authenticated(self, request):
    prepare_xml_response(request)
    serialization.streamer_metrics_to_xml(self.streamer, request)


class StreamerLogsResource(AuthenticationRequiredResource):

  def __init__(self, streamer):
    AuthenticationRequiredResource.__init__(self)
    self.streamer = streamer

  def render_GET_authenticated(self, request):
    prepare_xml_response(request)
    serialization.streamer_logs_to_xml(self.streamer.logs, request)


class SystemMetricsResource(AuthenticationRequiredResource):

  def __init__(self):
    AuthenticationRequiredResource.__init__(self)

  def render_GET_authenticated(self, request):
    prepare_xml_response(request)
    serialization.system_metrics_to_xml(utils.get_system_metrics(), request)


class StreamerRestartResource(AuthenticationRequiredResource):

  def __init__(self, streamer):
    AuthenticationRequiredResource.__init__(self)
    self.streamer = streamer

  def render_GET_authenticated(self, request):
    self.streamer.restart()
    return ""


class StreamerOrdersResource(AuthenticationRequiredResource):

  def __init__(self, streamer):
    AuthenticationRequiredResource.__init__(self)
    self.streamer = streamer

  def render_GET_authenticated(self, request):
    prepare_xml_response(request)
    buf = StringBuffer()
    buf.write("<orders>")
    for order in self.streamer.orders.values():
      serialization.order_to_xml(order, buf)
    buf.write("</orders>")
    return str(buf)

