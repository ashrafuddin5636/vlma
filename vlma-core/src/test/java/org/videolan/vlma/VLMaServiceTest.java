/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma;

import static org.junit.Assert.*;

import java.net.InetAddress;

import org.apache.commons.configuration.BaseConfiguration;
import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.videolan.vlma.exception.NotProgrammedException;
import org.videolan.vlma.model.Adapter;
import org.videolan.vlma.model.DTTChannel;
import org.videolan.vlma.model.FilesAdapter;
import org.videolan.vlma.model.Media;
import org.videolan.vlma.model.Program;
import org.videolan.vlma.model.Server;
import org.videolan.vlma.model.StreamingStrategy;
import org.videolan.vlma.util.Utils;

public class VLMaServiceTest {

    private Configuration configuration;
    private Utils utils;
    private Media media;
    private Program program;

    @Before
    public void setup() throws Exception {
        configuration = new BaseConfiguration();
        configuration.setProperty("vlma.streaming.rtsp.port", 4242);
        configuration.setProperty("vlma.streaming.http.port", 2424);
        utils = new Utils();
        utils.setConfiguration(configuration);
        media = new DTTChannel();
        media.setName("m1");
        program = new Program();
        program.setMedia(media);
        program.setIp(InetAddress.getByName("224.0.0.1"));
        Server server = new Server();
        Adapter adapter = new FilesAdapter();
        adapter.setName("pouet");
        adapter.setServer(server);
        server.addAdapter(adapter);
        program.setAdapter(adapter);
        program.setStreamingStrategy(new StreamingStrategy());
        media.addProgram(program);
    }

    @Test
    public void testHTTPProgramUrl() throws NotProgrammedException {
        program.getStreamingStrategy().setProtocol(StreamingStrategy.Protocol.HTTP);
        String url = utils.getUrl(program);
        assertTrue(url.startsWith("http://"));
    }

    @Test
    public void testVodProgramUrl() throws NotProgrammedException {
        program.getStreamingStrategy().setProtocol(StreamingStrategy.Protocol.RTSP);
        String url = utils.getUrl(program);
        assertTrue(url.startsWith("rtsp://"));
    }

    @Test
    public void testUDPProgramUrl() throws NotProgrammedException {
        program.getStreamingStrategy().setProtocol(StreamingStrategy.Protocol.UDP_MULTICAST);
        String url = utils.getUrl(program);
        assertTrue(url.startsWith("udp://@"));
    }
}
