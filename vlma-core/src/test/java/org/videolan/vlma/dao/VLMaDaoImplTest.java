/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.dao;

import java.net.UnknownHostException;

import junit.framework.TestCase;

import org.apache.commons.configuration.BaseConfiguration;
import org.apache.commons.configuration.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.videolan.vlma.model.DTTChannel;
import org.videolan.vlma.model.Media;
import org.videolan.vlma.model.Satellite;
import org.videolan.vlma.model.Server;

public class VLMaDaoImplTest extends TestCase {

    private VLMaDao vlmaDao;

    @Before
    public void setUp() {
        Configuration conf = new BaseConfiguration();
        conf.setProperty("vlma.data", System.getProperty("java.io.tmpdir"));
        this.vlmaDao = new VLMaDaoImpl();
        ((VLMaDaoImpl) vlmaDao).setConfiguration(conf);
    }

    @Test
    public void testAddMedia() {
        Media media = new DTTChannel();
        media.setName("channel");
        vlmaDao.add(media);
        assertEquals(1, vlmaDao.getMedias().size());
        assertEquals(media, vlmaDao.getMedias().get(0));
        assertNotNull(vlmaDao.getMedias().get(0).getId());
    }

    @Test
    public void testAddSatellite() {
        Satellite satellite = new Satellite("Hotbird");
        vlmaDao.add(satellite);
        assertEquals(1, vlmaDao.getSatellites().size());
        assertEquals(satellite, vlmaDao.getSatellites().get(0));
        assertNotNull(vlmaDao.getSatellites().get(0).getId());
    }

    @Test
    public void testAddServer() throws Exception {
        Server server = new Server();
        server.setName("vls0");
        server.setAddress("127.0.0.1");
        vlmaDao.add(server);
        assertEquals(1, vlmaDao.getServers().size());
        assertEquals(server, vlmaDao.getServers().get(0));
        assertNotNull(vlmaDao.getServers().get(0).getId());
    }

    @Test
    public void testGetMedia() {
        Media media = new DTTChannel();
        media.setName("channel");
        vlmaDao.add(media);
        assertEquals(media, vlmaDao.getMedia(media.getId()));
    }

    @Test
    public void testGetSatellite() {
        Satellite satellite = new Satellite("Hotbird");
        vlmaDao.add(satellite);
        assertEquals(satellite, vlmaDao.getSatellite(satellite.getId()));
    }

    @Test
    public void testGetServer() throws UnknownHostException {
        Server server = new Server();
        server.setName("vls0");
        vlmaDao.add(server);
        assertEquals(server, vlmaDao.getServer(server.getId()));
    }

    @Test
    public void testRemoveMedia() {
        Media media = new DTTChannel();
        vlmaDao.add(media);
        vlmaDao.remove(media);
        assertEquals(0, vlmaDao.getMedias().size());
    }

    @Test
    public void testRemoveSatellite() {
        Satellite satellite = new Satellite("Hotbird");
        vlmaDao.add(satellite);
        vlmaDao.remove(satellite);
        assertEquals(0, vlmaDao.getSatellites().size());
    }

    @Test
    public void testRemoveServer() throws UnknownHostException {
        Server server = new Server();
        server.setName("vls0");
        vlmaDao.add(server);
        vlmaDao.remove(server);
        assertEquals(0, vlmaDao.getServers().size());
    }

    @Test
    public void testUpdateMedia() {
        Media media1 = new DTTChannel();
        media1.setName("channel1");
        vlmaDao.add(media1);
        Media media2 = new DTTChannel();
        media2.setId(media1.getId());
        media2.setName("channel2");
        vlmaDao.update(media2);
        assertEquals(media2, vlmaDao.getMedia(media1.getId()));
    }

    @Test
    public void testUpdateSatellite() {
        Satellite satellite1 = new Satellite("Hotbird");
        vlmaDao.add(satellite1);
        Satellite satellite2 = new Satellite("Astra");
        satellite2.setId(satellite1.getId());
        vlmaDao.update(satellite2);
        assertEquals(satellite2, vlmaDao.getSatellite(satellite1.getId()));
    }

    @Test
    public void testUpdateServer() throws Exception {
        Server server1 = new Server();
        server1.setName("vls1");
        server1.setAddress("127.0.0.1");
        vlmaDao.add(server1);
        Server server2 = new Server();
        server2.setId(server1.getId());
        server2.setName("vls2");
        server2.setAddress("127.0.0.1");
        vlmaDao.update(server2);
        assertEquals(server2, vlmaDao.getServer(server1.getId()));
    }
}
