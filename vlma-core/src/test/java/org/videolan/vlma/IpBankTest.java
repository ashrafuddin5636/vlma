/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.BaseConfiguration;
import org.apache.commons.configuration.Configuration;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.videolan.vlma.dao.VLMaDao;
import org.videolan.vlma.model.Media;
import org.videolan.vlma.model.Program;
import org.videolan.vlma.model.StreamChannel;
import org.videolan.vlma.model.DTTChannel;
import org.videolan.vlma.util.ProgramFactory;

@RunWith(JMock.class)
public class IpBankTest {

    private Mockery context = new JUnit4Mockery();
    private Configuration configuration;
    private IpBank ipBank;
    private ProgramFactory factory;
    private VLMaDao vlmaDao;
    List<Media> medias;

    @Before
    public void setUp() throws Exception {
        vlmaDao = context.mock(VLMaDao.class);
        configuration = new BaseConfiguration();
        configuration.setProperty("vlma.streaming.udp.ipbank.min", "239.127.42.253");
        configuration.setProperty("vlma.streaming.udp.ipbank.max", "239.127.67.42");
        configuration.setProperty("vlma.announcement", "PODCAST");
        configuration.setProperty("vlma.streaming", "UDP_MULTICAST");
        configuration.setProperty("vlma.encapsulation", "TS");
        factory = new ProgramFactory();
        factory.setConfiguration(configuration);
        ipBank = new IpBank();
        ipBank.setVlmaDao(vlmaDao);
        ipBank.setConfiguration(configuration);
        Media media1 = new StreamChannel();
        media1.setName("Test1");
        Media media2 = new DTTChannel();
        media2.setName("Test2");
        Program program = factory.newProgram();
        program.setMedia(media2);
        program.setIp(InetAddress.getByName("239.127.62.15"));
        media2.addProgram(program);
        medias = new ArrayList<Media>();
        medias.add(media1);
        medias.add(media2);
        context.checking(new Expectations() {
            {
                one(vlmaDao).getMedias();
                will(returnValue(medias));
            }
        });
        ipBank.initIps();
    }

    @Test
    public void testInit() throws Exception {
        context.assertIsSatisfied();
        assertTrue(ipBank.isUsed(InetAddress.getByName("239.127.62.15")));
        assertFalse(ipBank.isUsed(InetAddress.getByName("239.127.12.42")));
    }

    @Test
    public void testGetIp() {
        InetAddress ip = ipBank.getIp();
        assertTrue(ipBank.isUsed(ip));
    }

    @Test
    public void testReleaseIp() {
        InetAddress ip = ipBank.getIp();
        assertTrue(ipBank.isUsed(ip));
        ipBank.releaseIp(ip);
        assertFalse(ipBank.isUsed(ip));
    }

}
