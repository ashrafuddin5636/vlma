/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.videolan.vlma.model.Adapter;
import org.videolan.vlma.model.AdapterFamily;
import org.videolan.vlma.model.FilesAdapter;
import org.videolan.vlma.model.FilesChannel;
import org.videolan.vlma.model.Media;
import org.videolan.vlma.model.Program;
import org.videolan.vlma.model.Server;
import org.videolan.vlma.model.StreamAdapter;
import org.videolan.vlma.order.management.OrderComputer;

import com.google.common.collect.ListMultimap;

public class OrderGiverTest extends TestCase {

    private OrderComputer orderGiver;

    public void setUp() {
        orderGiver = new OrderComputer();
    }

    public void testPartitionAdapters() throws UnknownHostException {
        Server s1 = new Server();
        Server s2 = new Server();
        List<Server> servers = new ArrayList<Server>();
        assertEquals(0, orderGiver.partitionAdapters(servers).size());
        servers.add(s1);
        servers.add(s2);
        assertEquals(0, orderGiver.partitionAdapters(servers).size());
        Adapter a1 = new FilesAdapter();
        a1.setName("a1");
        Adapter a2 = new StreamAdapter();
        a2.setName("a2");
        Adapter a3 = new StreamAdapter();
        a3.setName("a3");
        s1.getAdapters().add(a2);
        assertEquals(1, orderGiver.partitionAdapters(servers).keySet().size());
        assertEquals(1, orderGiver.partitionAdapters(servers).get(a2.getFamily()).size());
        s2.getAdapters().add(a3);
        assertEquals(a2.getFamily(), a3.getFamily());
        assertEquals(1, orderGiver.partitionAdapters(servers).keySet().size());
        assertEquals(2, orderGiver.partitionAdapters(servers).get(a2.getFamily()).size());
        s1.getAdapters().add(a1);
        assertEquals(2, orderGiver.partitionAdapters(servers).keySet().size());
        assertEquals(2, orderGiver.partitionAdapters(servers).get(a2.getFamily()).size());
        assertEquals(1, orderGiver.partitionAdapters(servers).get(a1.getFamily()).size());
    }

    public void testPartitionMedias() throws UnknownHostException {
        Server s1 = new Server();
        s1.setName("server1");
        Server s2 = new Server();
        List<Server> servers = new ArrayList<Server>();
        servers.add(s1);
        servers.add(s2);
        Adapter a1 = new FilesAdapter();
        a1.setName("a1");
        Adapter a2 = new StreamAdapter();
        a2.setName("a2");
        Adapter a3 = new StreamAdapter();
        a3.setName("a3");
        s1.getAdapters().add(a1);
        a1.setServer(s1);
        s1.getAdapters().add(a2);
        a2.setServer(s1);
        s2.getAdapters().add(a3);
        a3.setServer(s2);

        List<Media> medias = new ArrayList<Media>();
        FilesChannel m1 = new FilesChannel();
        m1.setServer(s1);
        m1.setName("m1");
        Program p1 = new Program();
        p1.setName("pouet");
        p1.setMedia(m1);
        m1.addProgram(p1);
        medias.add(m1);
        assertEquals(1, orderGiver.partitionMedias(medias,
                orderGiver.partitionAdapters(servers)).get(a1.getFamily()).size());
        assertTrue(orderGiver.partitionMedias(medias,
                orderGiver.partitionAdapters(servers)).get(a2.getFamily()).isEmpty());
        FilesChannel m2 = new FilesChannel();
        m2.setServer(s1);
        m2.setName("m2");
        Program p2 = new Program();
        p2.setName("plop");
        p2.setMedia(m2);
        m2.addProgram(p2);
        medias.add(m2);
        assertEquals(2, orderGiver.partitionMedias(medias,
                orderGiver.partitionAdapters(servers)).get(a1.getFamily()).size());
        assertTrue(orderGiver.partitionMedias(medias,
                orderGiver.partitionAdapters(servers)).get(a2.getFamily()).isEmpty());
    }

    public void testComputeOrders() throws UnknownHostException {
        Server s1 = new Server();
        s1.setName("server1");
        Server s2 = new Server();
        List<Server> servers = new ArrayList<Server>();
        servers.add(s1);
        servers.add(s2);
        Adapter a1 = new FilesAdapter();
        a1.setName("a1");
        Adapter a2 = new StreamAdapter();
        a2.setName("a2");
        Adapter a3 = new StreamAdapter();
        a3.setName("a3");
        s1.getAdapters().add(a1);
        a1.setServer(s1);
        s1.getAdapters().add(a2);
        a2.setServer(s1);
        s2.getAdapters().add(a3);
        a3.setServer(s2);

        List<Media> medias = new ArrayList<Media>();
        FilesChannel m1 = new FilesChannel();
        m1.setServer(s1);
        m1.setName("m1");
        Program p1 = new Program();
        p1.setName("pouet");
        p1.setMedia(m1);
        m1.addProgram(p1);
        medias.add(m1);
        FilesChannel m2 = new FilesChannel();
        m2.setServer(s1);
        m2.setName("m2");
        Program p2 = new Program();
        p2.setName("plop");
        p2.setMedia(m2);
        m2.addProgram(p2);
        medias.add(m2);

        ListMultimap<AdapterFamily, Adapter> adapters = orderGiver.partitionAdapters(servers);
        // No media => no order
        assertEquals(0,
                orderGiver.computeOrders(orderGiver.partitionMedias(new ArrayList<Media>(),
                        adapters), adapters).size());
        // No adapter => no order
        assertEquals(0,
                orderGiver.computeOrders(orderGiver.partitionMedias(medias,
                        orderGiver.partitionAdapters(new ArrayList<Server>())),
                        orderGiver.partitionAdapters(new ArrayList<Server>())).size());
        // One adapter and one mediagroup of the same getType => several orders
        assertTrue(0 < orderGiver.computeOrders(orderGiver.partitionMedias(medias,
                adapters), adapters).size());
    }

}
