/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.server.rpc;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import org.videolan.vlma.model.Server;

import snmp.SNMPSequence;
import snmp.SNMPVarBindList;
import snmp.SNMPv1CommunicationInterface;

/**
 * A data retriever using the SNMP protocol.
 * 
 * This data retriever is deprecated, please use the @link{HTTPDataRetriever} instead.
 */
public class SnmpDataRetriever implements DataRetriever {

    private static final Logger logger = Logger.getLogger(SnmpDataRetriever.class);

    private Configuration configuration;

    public Map<ServerState, Double> getServerState(Server server) {
        Map<ServerState, Double> result = new HashMap<ServerState, Double>();
        for(ServerState data : ServerState.values()) {
            double value;
            try {
                String stringValue = get(server, data);
                value = Double.parseDouble(stringValue);
            } catch(Exception e) {
                logger.error(e);
                value = 0D;
            }
            result.put(data, value);
        }
        return result;
    }

    private String get(Server server, ServerState data) throws Exception {
        return get(server, new String[] {configuration.getString("vlc.snmp.oid." + data.name().toLowerCase())});
    }

    private String get(Server server, String[] oid) throws Exception {
        SNMPv1CommunicationInterface comm = new SNMPv1CommunicationInterface(0, server.getIp(), configuration.getString("vlc.snmp.community"));
        SNMPVarBindList varList = comm.getMIBEntry(oid);
        SNMPSequence pair = (SNMPSequence) (varList.getSNMPObjectAt(0));
        return pair.getSNMPObjectAt(1).toString().trim();
    }

    public List<String> getVlcLogTail(Server server) throws IOException {
        try {
          String logTail = get(server, new String[] {configuration.getString("vlc.snmp.oid.vlc_logtail")});
          String[] lines = logTail.split("\n");
          return Arrays.asList(lines);
        } catch(Exception e) {
            throw new IOException(e.getMessage());
        }
    }

    public String getVlcVersion(Server server) throws IOException {
        try {
            return get(server, new String[] {configuration.getString("vlc.snmp.oid.vlc_version")});
          } catch(Exception e) {
              throw new IOException(e.getMessage());
          }
    }

    public boolean restartVlc(Server server) throws IOException {
        try {
            get(server, new String[] {configuration.getString("vlc.snmp.oid.vlc_restart")});
            return true;
          } catch(Exception e) {
              throw new IOException(e.getMessage());
          }
    }

    public Long getVlcUptime(Server server) throws IOException {
        try {
            return Long.parseLong(get(server,
                    new String[] {configuration.getString("vlc.snmp.oid.vlc_uptime")}));
          } catch(Exception e) {
              throw new IOException(e.getMessage());
          }
    }

    /**
     * @param configuration the configuration to set
     */
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

}
