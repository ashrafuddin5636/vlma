/*
 * Copyright (C) 2008-2009 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.server.rpc;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import org.videolan.vlma.model.Server;

public class HttpDataRetriever implements DataRetriever {

    private static final Logger logger = Logger.getLogger(HttpDataRetriever.class);

    private Configuration configuration;

    private InputStream getPageContent(Server server, String path) throws IOException {
        URL url = new URL("http://" + server.getIp().getHostName() + ":"
                + configuration.getInt("vlc.monitor.http.port") + path);
        URLConnection conn = url.openConnection();
        String userPassword = configuration.getString("vlc.monitor.http.login") + ":"
                + configuration.getString("vlc.monitor.http.password");
        String encodedUserPassword = new String(Base64.encodeBase64(userPassword.getBytes()));
        conn.setRequestProperty("Authorization", "Basic " + encodedUserPassword);
        return conn.getInputStream();
    }

    public Map<ServerState, Double> getServerState(Server server) {
        Map<ServerState, Double> result = new HashMap<ServerState, Double>();
        for(ServerState data : ServerState.values()) {
            result.put(data, 0D);
        }
        HttpDataReader rd = null;
        try {
            InputStream is = getPageContent(server, "/system/monitor/?format=raw");
            rd = new HttpDataReader(is);
        } catch (IOException e) {
            logger.error(e);
            return result;
        }
        while (true) {
            String key;
            try {
                key = rd.readString();
                if (key == null) break;
                try {
                    ServerState data = ServerState.valueOf(key);
                    Double value = rd.readDouble();
                    result.put(data, value);
                } catch (IOException e) {
                    logger.error(e);
                }
            } catch (IOException e1) {
                logger.error(e1);
                break;
            }
        }
        try {
            rd.close();
        } catch (IOException e) {
            logger.error(e);
        }
        return result;
    }

    public List<String> getVlcLogTail(Server server) throws IOException {
        InputStream is = getPageContent(server, "/vlc/logTail/?format=raw");
        HttpDataReader rd = new HttpDataReader(is);
        try {
            List<String> result = new LinkedList<String>();
            String s;
            while ((s = rd.readString()) != null) {
                result.add(s);
            }
            return result;
        } finally {
            rd.close();
        }
    }

    public String getVlcVersion(Server server) throws IOException {
        InputStream is = getPageContent(server, "/vlc/version/?format=raw");
        HttpDataReader rd = new HttpDataReader(is);
        try {
            return rd.readString();
        } finally {
            rd.close();
        }
    }

    public boolean restartVlc(Server server) throws IOException {
        InputStream is = getPageContent(server, "/vlc/restart/?format=raw");
        return is != null;
    }

    public Long getVlcUptime(Server server) throws IOException {
        InputStream is = getPageContent(server, "/vlc/uptime/?format=raw");
        HttpDataReader rd = new HttpDataReader(is);
        try {
            return rd.readLong();
        } finally {
            rd.close();
        }
    }

    /**
     * @param configuration the configuration to set
     */
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

}
