/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.notifier;

import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;

/**
 * A mail notifier.
 *
 * @author Adrien Grand <jpountz at videolan.org>
 */
public class MailNotifier extends Notifier {

    private static final Logger logger = Logger.getLogger(MailNotifier.class);

    private Configuration configuration;

    private JavaMailSenderImpl mailSender;

    /**
     * Sets the configuration.
     *
     * @param configuration the configuration to set
     */
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    /**
     * Sets the mail sender.
     *
     * @param mailSender
     */
    public void setMailSender(JavaMailSenderImpl mailSender) {
        this.mailSender = mailSender;
    }

    /**
     * Is mail notification enabled?
     *
     * @return true if and only if there is one recipient ore more
     */
    @SuppressWarnings("unchecked")
    private boolean isMailEnabled() {
        List<String> tmp = configuration.getList("vlma.notification.mail.recipients");
        return (tmp != null && tmp.size() > 0);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void sendNotification(String message) {
        if (isMailEnabled()) {
            SimpleMailMessage msg = new SimpleMailMessage();
            String from = configuration.getString("vlma.notification.mail.sender");
            List<String> recipients = configuration.getList("vlma.notification.mail.recipients");
            mailSender.setHost(configuration.getString("vlma.notification.mail.host"));
            msg.setFrom(from);
            String[] recipientsArray = new String[recipients.size()];
            int i = 0;
            for (String recipient : recipients) {
                recipientsArray[i++] = recipient;
            }
            msg.setTo(recipientsArray);
            msg.setSubject(message);
            msg.setText(message);
            try{
                this.mailSender.send(msg);
            }
            catch(MailException e) {
                // TODO: Do better?
                logger.error("Mail could not be sent.", e);
            }
        }
    }

}
