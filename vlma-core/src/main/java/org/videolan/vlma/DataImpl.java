/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma;

import java.io.IOException;
import java.net.InetAddress;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import org.videolan.vlma.daemon.Daemon;
import org.videolan.vlma.dao.VLMaDao;
import org.videolan.vlma.model.Command;
import org.videolan.vlma.model.Media;
import org.videolan.vlma.model.Program;
import org.videolan.vlma.model.SatChannel;
import org.videolan.vlma.model.Satellite;
import org.videolan.vlma.model.Server;
import org.videolan.vlma.monitor.ServerStateMonitor;
import org.videolan.vlma.order.management.OrderManager;
import org.videolan.vlma.order.sender.CommandLogger;
import org.videolan.vlma.order.sender.TelnetConnection;
import org.videolan.vlma.server.rpc.DataRetriever;
import org.videolan.vlma.util.ProgramFactory;
import org.videolan.vlma.util.SatcodxUtils;
import org.videolan.vlma.util.Utils;

/**
 * Implementation of the Data interface.
 *
 * @author Adrien Grand <jpountz at videolan.org>
 */
public class DataImpl implements Data {

    private static final Logger logger = Logger.getLogger(DataImpl.class);

    private CommandLogger commandLogger;
    private Configuration configuration;
    private Daemon daemon;
    private IpBank ipBank;
    private ProgramFactory programFactory;
    private VLMaDao vlmaDao;
    private DataRetriever dataRetriever;
    private OrderManager orderManager;
    private ServerStateMonitor serverStateMonitor;
    private Utils utils;

    /*************************************************************************
     *
     * Interaction with VLMa daemon
     *
     *************************************************************************/

    public void reload() {
        daemon.reload();
    }

    public void stop() {
        daemon.stop();
    }

    /*************************************************************************
     *
     * Management of VLMa configuration
     *
     *************************************************************************/

    /*
     * Configuration is not exposed directly because it is not serializable
     */

    public String getString(String key) {
        synchronized (configuration) {
            return configuration.getString(key);
        }
    }

    public Integer getInt(String key) {
        synchronized (configuration) {
            return configuration.getInt(key);
        }
    }

    // Documentation says Configuration#getList returns a list of strings
    // See http://commons.apache.org/configuration/apidocs/index.html
    @SuppressWarnings("unchecked")
    public List<String> getList(String key) {
        synchronized (configuration) {
            return configuration.getList(key);
        }
    }

    public void setProperty(String key, Object value) {
        synchronized (configuration) {
            configuration.setProperty(key, value);
        }
    }

    public void clearProperty(String key) {
        synchronized (configuration) {
            configuration.clearProperty(key);
        }
    }

    /*************************************************************************
     *
     * Data Access Objects
     *
     *************************************************************************/

    public List<Media> getMedias() {
        return vlmaDao.getMedias();
    }

    public List<Satellite> getSatellites() {
        return vlmaDao.getSatellites();
    }

    public List<Server> getServers() {
        return vlmaDao.getServers();
    }

    public Media getMedia(int id) {
        return vlmaDao.getMedia(id);
    }

    public Satellite getSatellite(int id) {
        return vlmaDao.getSatellite(id);
    }

    public Server getServer(int id) {
        return vlmaDao.getServer(id);
    }

    public void update(Media media) {
        Media m = vlmaDao.getMedia(media.getId());
        Set<InetAddress> ipsToRelease = new HashSet<InetAddress>();
        for (Program program : m.getPrograms()) {
            if (program.getIp() != null) {
                ipsToRelease.add(program.getIp());
            }
        }
        for (Program program : media.getPrograms()) {
            switch (program.getStreamingStrategy().getProtocol()) {
            case UDP_MULTICAST:
                if (program.getIp() == null) {
                    program.setIp(ipBank.getIp());
                }
                ipsToRelease.remove(program.getIp());
                break;
            default:
                if (program.getIp() != null) {
                    ipBank.releaseIp(program.getIp());
                    program.setIp(null);
                }
                break;
            }
        }
        for (InetAddress ip : ipsToRelease) {
            ipBank.releaseIp(ip);
        }
        vlmaDao.update(media);
    }

    public void update(Satellite satellite) {
        vlmaDao.update(satellite);
    }

    public void update(Server server) {
        vlmaDao.update(server);
    }

    public void add(Media media) {
        vlmaDao.add(media);
    }

    public void add(Satellite satellite) {
        vlmaDao.add(satellite);
    }

    public void add(Server server) {
        vlmaDao.add(server);
    }

    public void remove(Media media) {
        vlmaDao.remove(media);
    }

    public void remove(Satellite satellite) {
        vlmaDao.remove(satellite);
    }

    public void remove(Server server) {
        vlmaDao.remove(server);
    }

    /*************************************************************************
     *
     * Various utilities
     *
     *************************************************************************/

    public String getUrl(Program program) {
        return utils.getUrl(program);
    }

    public Program newProgram() {
        return programFactory.newProgram();
    }

    public void updateSatChannels(URL source) throws IOException {
        Set<Media> newMedias = new HashSet<Media>();
        Configuration satcodxConfiguration = configuration.subset("satcodx");

        SatcodxUtils.getSatChannels(source, satcodxConfiguration, newMedias);

        Set<String> coverages = new HashSet<String>();
        List<Media> medias = vlmaDao.getMedias();
        for (Media ch : newMedias) {
            logger.debug("Adding " + ch.getName());
            coverages.add(((SatChannel)ch).getCoverage());
            for (Media media : medias) {
                if (ch.equals(media)) {
                    ch.setPrograms(media.getPrograms());
                }
            }
        }

        logger.debug("Got " + newMedias.size() + " channels");
        List<Media> mediasToRemove = new ArrayList<Media>();
        for (Media ch : medias) {
            if (ch instanceof SatChannel && coverages.contains(((SatChannel)ch).getCoverage()))
                mediasToRemove.add(ch);
        }
        vlmaDao.removeAll(mediasToRemove);
        vlmaDao.addAll(newMedias);

        logger.info("Ending analysis of " + source.toString());
    }

    public void giveOrders() {
        new Thread() {
            public void run() {
                orderManager.updateOrders();
                orderManager.giveOrders();
            }
        }.start();
    }

    public List<Command> getCommands() {
        return commandLogger.getCommands();
    }

    private class VlcChecker extends Thread {

        public VlcChecker() {
            this.setName("VLC checker");
        }

        public void run() {
            serverStateMonitor.checkVLCs();
        }
    }

    public void startCheckAllVLCs() {
        Thread vlcChecker = new VlcChecker();
        vlcChecker.start();
    }

    public List<String> getVlcLogTail(Server server) throws RemoteException, IOException {
        return dataRetriever.getVlcLogTail(server);
    }

    public String getVlcVersion(Server server) throws RemoteException, IOException {
        return dataRetriever.getVlcVersion(server);
    }

    public void restartVlc(Server server) throws RemoteException, IOException {
        dataRetriever.restartVlc(server);
    }

    public Long getVlcUptime(Server server) throws RemoteException, IOException {
        return dataRetriever.getVlcUptime(server);
    }

    // TODO: Do not log in every time, use sessions
    public String getVlcResponse(Server server, String command) throws RemoteException, IOException {
        TelnetConnection conn = new TelnetConnection();
        conn.connect(server.getIp(), configuration.getInt("vlc.telnet.port"));
        conn.println(configuration.getString("vlc.telnet.password"));
        conn.waitUntilReady();
        conn.resetIn();
        String response = conn.run(command);
        conn.close();
        return response;
    }

    /*************************************************************************
     *
     * Setters
     *
     *************************************************************************\

    /**
     * Sets the configuration.
     *
     * @param configuration the configuration to set
     */
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    /**
     * Sets the daemon.
     *
     * @param daemon the daemon to set
     */
    public void setDaemon(Daemon daemon) {
        this.daemon = daemon;
    }

    /**
     * Sets the command logger.
     *
     * @param commandLogger the commandLogger to set
     */
    public void setCommandLogger(CommandLogger commandLogger) {
        this.commandLogger = commandLogger;
    }

    /**
     * Sets the IP bank.
     *
     * @param ipBank the ipBank to set
     */
    public void setIpBank(IpBank ipBank) {
        this.ipBank = ipBank;
    }

    /**
     * Sets the program factory
     *
     * @param programFactory the programFactory to set
     */
    public void setProgramFactory(ProgramFactory programFactory) {
        this.programFactory = programFactory;
    }

    /**
     * Sets VLMa DAO
     *
     * @param vlmaDao the vlmaDao to set
     */
    public void setVlmaDao(VLMaDao vlmaDao) {
        this.vlmaDao = vlmaDao;
    }

    /**
     * Sets the data retriever.
     *
     * @param dataRetriever the dataRetriever to set
     */
    public void setDataRetriever(DataRetriever dataRetriever) {
        this.dataRetriever = dataRetriever;
    }

    /**
     * Sets the order manager.
     *
     * @param orderManager the orderManager to set
     */
    public void setOrderManager(OrderManager orderManager) {
        this.orderManager = orderManager;
    }

    /**
     * Sets the server's state monitor.
     *
     * @param serverStateMonitor the serverStateMonitor to set
     */
    public void setServerStateMonitor(ServerStateMonitor serverStateMonitor) {
        this.serverStateMonitor = serverStateMonitor;
    }

    /**
     * Sets utils.
     *
     * @param utils the utils to set
     */
    public void setUtils(Utils utils) {
        this.utils = utils;
    }

}
