/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.order.sender;

import java.io.IOException;

import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import org.videolan.vlma.model.Adapter;
import org.videolan.vlma.model.Order;
import org.videolan.vlma.model.Server;
import org.videolan.vlma.order.vlm.OrderBuilder;
import org.videolan.vlma.order.vlm.VLMBroadcast;
import org.videolan.vlma.order.vlm.VLMOrder;
import org.videolan.vlma.order.vlm.VLMVod;

/**
 * This class represents an order given to a VLC server.
 * It is associated with a media group.
 *
 * @author Sylvain Cadilhac <sylv at videolan.org>
 */
public class OrderSender {

    private static final Logger logger = Logger.getLogger(OrderSender.class);

    protected CommandLogger commandLogger;

    protected Configuration configuration;

    private OrderBuilder orderBuilder;

    /**
     * Connects to the VLC server using telnet.
     *
     * @throws IOException Connection aborted.
     */
    public TelnetConnection getConn(Server server) throws IOException {
        TelnetConnection conn = new TelnetConnection();
        conn.connect(server.getIp(), configuration.getInt("vlc.telnet.port"));
        conn.println(configuration.getString("vlc.telnet.password"));
        conn.waitUntilReady();
        // Clear the input stream so as not to read the password later
        conn.resetIn();
        return conn;
    }

    /**
     * Sends a command to to VLC server.
     *
     * @param command the command to send
     * @throws IOException Operation aborted.
     */
    private void telnetCommand(TelnetConnection conn, String command, Adapter adapter) throws IOException {
        if (logger.isDebugEnabled()) {
            logger.debug("Send command " + command + " to " + adapter.getServer().getName());
        }
        String response = conn.run(command);
        commandLogger.add(adapter.getServer(), command, response);
        if (logger.isDebugEnabled()) {
            logger.debug("Command result: " + response);
        }
    }

    /**
     * Sends the order.
     *
     * @throws IOException Operation aborted.
     */
    public void start(TelnetConnection conn, Order order) throws IOException {
        Adapter adapter = order.getAdapter();
        VLMOrder o = orderBuilder.buildOrder(order);
        stopCommand(conn, o.getName(), adapter);
        if (o instanceof VLMBroadcast) {
            telnetCommand(conn, "new " + o.getName() + " broadcast enabled", adapter);
        } else if (o instanceof VLMVod) {
            telnetCommand(conn, "new " + o.getName() + " vod enabled", adapter);
        } else {
            assert false : "Unexpected VLMOrder";
        }
        for (String input : o.getInputs()) {
            telnetCommand(conn, String.format("setup %s input \"%s\"", o.getName(), input), adapter);
        }
        if (o instanceof VLMBroadcast && ((VLMBroadcast) o).isLoop()) {
            telnetCommand(conn, String.format("setup %s loop", o.getName()), adapter);
        }
        for (String option : o.getOptions()) {
            telnetCommand(conn, String.format("setup %s option %s", o.getName(), option), adapter);
        }
        if (o instanceof VLMBroadcast) {
            telnetCommand(conn, String.format("setup %s output %s", o.getName(), o.getSout()), adapter);
        }
        telnetCommand(conn, String.format("control %s play", o.getName()), adapter);
    }

    private void stopCommand(TelnetConnection conn, String commandName, Adapter adapter) throws IOException {
        telnetCommand(conn, "setup " + commandName + " disabled", adapter);
        telnetCommand(conn, "control " + commandName + " stop", adapter);
        telnetCommand(conn, "del " + commandName, adapter);
    }

    /**
     * Orders a server to stop streaming.
     *
     * @throws IOException
     */
    public void stop(TelnetConnection conn, Order order) throws IOException {
        String commandName = order.getName();
        Adapter adapter = order.getAdapter();
        telnetCommand(conn, "control " + commandName + " disabled", adapter);
        telnetCommand(conn, "control " + commandName + " stop", adapter);
        telnetCommand(conn, "del " + commandName, adapter);
    }

    /**
     * Sets the configuration.
     *
     * @param configuration the configuration to set
     */
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    /**
     * Sets the command logger.
     *
     * @param commandLogger the commandLogger to set
     */
    public void setCommandLogger(CommandLogger commandLogger) {
        this.commandLogger = commandLogger;
    }

    /**
     * Sets the order builder.
     *
     * @param orderBuilder
     */
    public void setOrderBuilder(OrderBuilder orderBuilder) {
        this.orderBuilder = orderBuilder;
    }

}
