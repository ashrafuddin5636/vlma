/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.order.vlm;

import java.util.ArrayList;
import java.util.List;

public abstract class VLMOrder {

    public static final String DVB_INPUT = "dvb://";

    protected String name;
    protected List<String> inputs;
    protected String sout;
    protected List<String> options;

    public VLMOrder() {
        inputs = new ArrayList<String>();
        options = new ArrayList<String>();
    }

    public void addInput(String input) {
        inputs.add(input);
    }

    public List<String> getInputs() {
        return inputs;
    }

    public void setInputs(List<String> inputs) {
        this.inputs = inputs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getOptions() {
        return options;
    }

    public void addOption(String option) {
        options.add(option);
    }

    public void setOptions(List<String> options) {
        this.options = options;
    }

    public String getSout() {
        return sout;
    }

    public void setSout(String sout) {
        this.sout = sout;
    }

}
