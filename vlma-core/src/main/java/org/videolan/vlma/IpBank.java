/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import org.videolan.vlma.dao.VLMaDao;
import org.videolan.vlma.exception.NotFoundException;
import org.videolan.vlma.model.Media;
import org.videolan.vlma.model.Program;

/**
 * A set of multicast IP addresses. This set is used to give addresses to start
 * a new programmation.
 *
 * @author Sylvain Cadilhac <sylv at videolan.org>
 */
public class IpBank {

    private static final Logger logger = Logger.getLogger(IpBank.class);

    private Set<InetAddress> usedIps;

    private VLMaDao vlmaDao;

    private Configuration configuration;

    private Inet4Address min, max;

    /**
     * Initializes IP addresses according to the data interface.
     *
     * @throws UnknownHostException
     *             address is invalid
     */
    synchronized public void initIps() throws UnknownHostException {
        usedIps = new HashSet<InetAddress>();

        min = (Inet4Address) InetAddress.getByName(configuration.getString("vlma.streaming.udp.ipbank.min"));
        max = (Inet4Address) InetAddress.getByName(configuration.getString("vlma.streaming.udp.ipbank.max"));

        for (Media m : vlmaDao.getMedias()) {
            for (Program program : m.getPrograms()) {
                logger.debug("The media " + m.getName() + " has programs");
                switch (program.getStreamingStrategy().getProtocol()) {
                case UDP_MULTICAST:
                    usedIps.add(program.getIp());
                }
            }
        }
    }

    synchronized public boolean isUsed(InetAddress ip) {
        return usedIps.contains(ip);
    }

    /**
     * Gives a new IP address to the programmation, the first which is not used.
     *
     * @return the IP address
     */
    synchronized public InetAddress getIp() {
        for(int ip = ipv42int(min); ip < ipv42int(max); ip++) {
            InetAddress i = null;
            try {
                i = int2ipv4(ip);
            } catch (UnknownHostException e) {
                // Can not happen since raw IPs are provided
                assert(false);
            }
            if(usedIps.add(i))
                return i;
        }
        throw new NotFoundException("No free IP available");
    }

    /**
     * Releases an IP address.
     *
     * @param ip
     */
    synchronized public void releaseIp(InetAddress ip) {
        if(ip == null) return;
        usedIps.remove(ip);
    }

    /**
     * Converts an IPv4 address to an integer.
     *
     * @param ip the IPv4 address
     * @return the associated integer
     */
    private int ipv42int(Inet4Address ip) {
        int result = 0;
        byte[] t = ip.getAddress();
        for (int i = 0; i < 4; i++) {
            result += (t[i] & 255) << (8 * (3 - i));
        }
        return result;
    }

    /**
     * Converts an integer to a IPv4 address.
     *
     * @param ip the integer
     * @return the associated IPv4
     * @throws UnknownHostException
     */
    private Inet4Address int2ipv4 (int ip) throws UnknownHostException {
        byte[] result = new byte[4];
        for (int i = 0; i < 4; i++) {
            result[i] = (byte) ((ip >> (8 * (3 - i))) & 255);
        }
        return (Inet4Address) Inet4Address.getByAddress(result);
    }

    /**
     * Sets the VLMa dao.
     *
     * @param vlmaDao the vlmaDao to set
     */
    public void setVlmaDao(VLMaDao vlmaDao) {
        this.vlmaDao = vlmaDao;
    }

    /**
     * Sets the configuration.
     *
     * @param configuration the configuration to set
     */
    public synchronized void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

}
