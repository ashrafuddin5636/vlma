/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.controller.server;

import java.rmi.RemoteException;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.videolan.vlma.Data;
import org.videolan.vlma.model.Adapter;
import org.videolan.vlma.model.Server;

public class ServerAdapterRemoveValidator implements Validator {

    private Data data;

    @SuppressWarnings("unchecked")
    public boolean supports(Class arg0) {
        return (arg0.equals(ServerAdapterAdd.class));
    }

    public void validate(Object arg0, Errors arg1) {
        ServerAdapterAdd serversAdapterRemove = (ServerAdapterAdd) arg0;

        if (serversAdapterRemove == null) {
            arg1.rejectValue("name",
                    "servers.adapter.remove.error.not-specified");
            return;
        } else {
            String adapterName = serversAdapterRemove.getName().trim();
            Server server = null;
            try {
                server = data.getServer(serversAdapterRemove.getServer());
            } catch (RemoteException e) {
                arg1.rejectValue("name", "error.remote_exception");
                return;
            }
            Adapter adapter = null;
            for(Adapter a : server.getAdapters()) {
                if (a.getName().equals(adapterName)) {
                    adapter = a;
                    break;
                }
            }
            if (adapter == null) {
                arg1.rejectValue("name", "servers.adapter.remove.error.nonexisting");
            }
        }
    }

    /**
     * @param data the data to set
     */
    public void setData(Data data) {
        this.data = data;
    }

}
