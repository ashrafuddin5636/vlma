/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.controller.media.file;

import java.rmi.RemoteException;
import java.util.Collection;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.videolan.vlma.Data;
import org.videolan.vlma.model.FilesChannel;
import org.videolan.vlma.model.Media;

public class FileChannelAddValidator implements Validator {

    private Data data;

    @SuppressWarnings("unchecked")
    public boolean supports(Class arg0) {
        return arg0.equals(FileChannelAdd.class);
    }

    public void validate(Object arg0, Errors arg1) {
        FileChannelAdd filesChannelsAdd = (FileChannelAdd) arg0;

        if ("".equals(filesChannelsAdd.getName())) {
            arg1.rejectValue("name", "medias.fileschannel.add.error.name_not_specified");
            return;
        }
        if ("".equals(filesChannelsAdd.getFiles())) {
            arg1.rejectValue("files", "medias.fileschannel.add.error.files_not_specified");
            return;
        }

        Collection<Media> medias;
        try {
            medias = data.getMedias();
            if (medias != null)
            {
                FilesChannel filesChannel = new FilesChannel();
                filesChannel.setName(filesChannelsAdd.getName());
                if (medias.contains(filesChannel)) {
                    arg1.rejectValue("name", "medias.fileschannel.add.error.existing_name");
                    return;
                }
            }
        } catch (RemoteException e) {
            arg1.rejectValue("files", "error.remote_exception");
        }

    }

    /**
     * @param data the data to set
     */
    public void setData(Data data) {
        this.data = data;
    }

}
