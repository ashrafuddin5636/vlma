/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.controller.media.stream;

import java.rmi.RemoteException;
import java.util.Collection;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.videolan.vlma.Data;
import org.videolan.vlma.model.Media;
import org.videolan.vlma.model.StreamChannel;

public class StreamChannelAddValidator implements Validator {

    private Data data;

    @SuppressWarnings("unchecked")
    public boolean supports(Class arg0) {
        return arg0.equals(StreamChannelAdd.class);
    }

    public void validate(Object arg0, Errors arg1) {
        StreamChannelAdd streamChannelsAdd = (StreamChannelAdd) arg0;

        if ("".equals(streamChannelsAdd.getName())) {
            arg1.rejectValue("name", "medias.streamchannel.add.error.name_not_specified");
            return;
        }
        if ("".equals(streamChannelsAdd.getStreamURL())) {
            arg1.rejectValue("streamURL", "medias.streamchannel.add.error.URL_not_specified");
            return;
        }

        Collection<Media> medias;
        try {
            medias = data.getMedias();
            if (medias != null)
            {
                StreamChannel streamChannel = new StreamChannel();
                streamChannel.setName(streamChannelsAdd.getName());
                for (Media media : medias) {
                    if (media.getName().equals(streamChannelsAdd.getName())) {
                        arg1.rejectValue("name", "medias.streamchannel.add.error.existing_name");
                        return;
                    }
                }
            }
        } catch (RemoteException e) {
            arg1.rejectValue("name", "error.remote_exception");
        }

    }

    /**
     * @param data the data to set
     */
    public void setData(Data data) {
        this.data = data;
    }

}
