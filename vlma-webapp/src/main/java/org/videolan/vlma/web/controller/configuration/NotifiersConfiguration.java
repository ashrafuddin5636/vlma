/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.controller.configuration;

public class NotifiersConfiguration {

    private String vlma_notification_mail_host;
    private String vlma_notification_mail_sender;
    private String vlma_notification_mail_recipients;
    private String vlma_notification_irc_host;
    private Integer vlma_notification_irc_port;
    private String vlma_notification_irc_nick;
    private String vlma_notification_irc_pass;
    private String vlma_notification_irc_chan;
    private String vlma_notification_msn_login;
    private String vlma_notification_msn_pass;
    private String vlma_notification_msn_recipients;

    public String getVlma_notification_irc_chan() {
        return vlma_notification_irc_chan;
    }
    public void setVlma_notification_irc_chan(String vlma_notification_irc_chan) {
        this.vlma_notification_irc_chan = vlma_notification_irc_chan;
    }
    public String getVlma_notification_irc_host() {
        return vlma_notification_irc_host;
    }
    public void setVlma_notification_irc_host(String vlma_notification_irc_host) {
        this.vlma_notification_irc_host = vlma_notification_irc_host;
    }
    public String getVlma_notification_irc_nick() {
        return vlma_notification_irc_nick;
    }
    public void setVlma_notification_irc_nick(String vlma_notification_irc_nick) {
        this.vlma_notification_irc_nick = vlma_notification_irc_nick;
    }
    public String getVlma_notification_irc_pass() {
        return vlma_notification_irc_pass;
    }
    public void setVlma_notification_irc_pass(String vlma_notification_irc_pass) {
        this.vlma_notification_irc_pass = vlma_notification_irc_pass;
    }
    public Integer getVlma_notification_irc_port() {
        return vlma_notification_irc_port;
    }
    public void setVlma_notification_irc_port(Integer vlma_notification_irc_port) {
        this.vlma_notification_irc_port = vlma_notification_irc_port;
    }
    public String getVlma_notification_mail_host() {
        return vlma_notification_mail_host;
    }
    public void setVlma_notification_mail_host(String vlma_notification_mail_host) {
        this.vlma_notification_mail_host = vlma_notification_mail_host;
    }
    public String getVlma_notification_mail_recipients() {
        return vlma_notification_mail_recipients;
    }
    public void setVlma_notification_mail_recipients(String vlma_notification_mail_recipients) {
        this.vlma_notification_mail_recipients = vlma_notification_mail_recipients;
    }
    public String getVlma_notification_mail_sender() {
        return vlma_notification_mail_sender;
    }
    public void setVlma_notification_mail_sender(
            String vlma_notification_mail_sender) {
        this.vlma_notification_mail_sender = vlma_notification_mail_sender;
    }
    public String getVlma_notification_msn_login() {
        return vlma_notification_msn_login;
    }
    public void setVlma_notification_msn_login(String vlma_notification_msn_login) {
        this.vlma_notification_msn_login = vlma_notification_msn_login;
    }
    public String getVlma_notification_msn_pass() {
        return vlma_notification_msn_pass;
    }
    public void setVlma_notification_msn_pass(String vlma_notification_msn_pass) {
        this.vlma_notification_msn_pass = vlma_notification_msn_pass;
    }
    public String getVlma_notification_msn_recipients() {
        return vlma_notification_msn_recipients;
    }
    public void setVlma_notification_msn_recipients(String vlma_notification_msn_recipients) {
        this.vlma_notification_msn_recipients = vlma_notification_msn_recipients;
    }

}