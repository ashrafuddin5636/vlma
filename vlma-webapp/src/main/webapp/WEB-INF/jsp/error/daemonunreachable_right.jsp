<%@ include file="/WEB-INF/jsp/include.jsp" %>

<h1><fmt:message key="error.page" /></h1>

<p class="error">
<fmt:message key="error.remotelookupfailureexception" />
</p>