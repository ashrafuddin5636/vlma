<%@ include file="/WEB-INF/jsp/include.jsp" %>

<ul>
    <li><a href="streamingconfiguration.htm"><fmt:message key="configuration.streaming" /></a></li>
    <li><a href="notifiersconfiguration.htm"><fmt:message key="configuration.notifiers" /></a></li>
</ul>
