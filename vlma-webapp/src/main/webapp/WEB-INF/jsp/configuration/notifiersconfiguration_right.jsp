<%@ include file="/WEB-INF/jsp/include.jsp" %>

<h1><fmt:message key="configuration.page" /></h1>

<form method="post">

<h2><fmt:message key="configuration.vlma.notification" /></h2>

<h3><fmt:message key="configuration.vlma.notification.mail" /></h3>

<table class="configuration fullwidth">
    <tr>
        <td><fmt:message key="vlma.notification.mail.host" /></td>
        <spring:bind path="notifiersConfiguration.vlma_notification_mail_host">
            <td><input name="vlma_notification_mail_host" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td><fmt:message key="vlma.notification.mail.sender" /></td>
        <spring:bind path="notifiersConfiguration.vlma_notification_mail_sender">
            <td><input name="vlma_notification_mail_sender" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td><fmt:message key="vlma.notification.mail.recipients" /></td>
        <spring:bind path="notifiersConfiguration.vlma_notification_mail_recipients">
            <td><input name="vlma_notification_mail_recipients" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
</table>

<h3><fmt:message key="configuration.vlma.notification.irc" /></h3>

<table class="configuration fullwidth">
    <tr>
        <td><fmt:message key="vlma.notification.irc.host" /></td>
        <spring:bind path="notifiersConfiguration.vlma_notification_irc_host">
            <td><input name="vlma_notification_irc_host" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td><fmt:message key="vlma.notification.irc.port" /></td>
        <spring:bind path="notifiersConfiguration.vlma_notification_irc_port">
            <td><input name="vlma_notification_irc_port" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td><fmt:message key="vlma.notification.irc.nick" /></td>
        <spring:bind path="notifiersConfiguration.vlma_notification_irc_nick">
            <td><input name="vlma_notification_irc_nick" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td><fmt:message key="vlma.notification.irc.pass" /></td>
        <spring:bind path="notifiersConfiguration.vlma_notification_irc_pass">
            <td><input name="vlma_notification_irc_pass" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td><fmt:message key="vlma.notification.irc.chan" /></td>
        <spring:bind path="notifiersConfiguration.vlma_notification_irc_chan">
            <td><input name="vlma_notification_irc_chan" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
</table>

<h3><fmt:message key="configuration.vlma.notification.msn" /></h3>

<table class="configuration fullwidth">
    <tr>
        <td><fmt:message key="vlma.notification.msn.login" /></td>
        <spring:bind path="notifiersConfiguration.vlma_notification_msn_login">
            <td><input name="vlma_notification_msn_login" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td><fmt:message key="vlma.notification.msn.pass" /></td>
        <spring:bind path="notifiersConfiguration.vlma_notification_msn_pass">
            <td><input name="vlma_notification_msn_pass" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
    <tr>
        <td><fmt:message key="vlma.notification.msn.recipients" /></td>
        <spring:bind path="notifiersConfiguration.vlma_notification_msn_recipients">
            <td><input name="vlma_notification_msn_recipients" type="text" value="<c:out value="${status.value}"/>" /></td>
            <td><span class="error"><c:out value="${status.errorMessage}" /></span></td>
        </spring:bind>
    </tr>
</table>

<input type="submit" value="<fmt:message key="configuration.save.button" />" />
</form>

<script type="text/javascript">
function resizeTables() {
    var tables = document.getElementsByClassName("configuration");
    for(var i=0; i<tables.length; i++) {
        resizeTable(tables[i]);
    }
}

function resizeTable(table) {
    var tds = table.getElementsByTagName("td");
    tds[0].style.width="30%";
    tds[1].style.width="50%";
    var inputs = table.getElementsByTagName("input");
    for(var j=0; j<inputs.length; j++) {
        inputs[j].parentNode.style.textAlign = "right";
        inputs[j].style.width = "88%";
    }
    var selects = table.getElementsByTagName("select");
    for(var j=0; j<selects.length; j++) {
        selects[j].parentNode.style.textAlign = "right";
        selects[j].style.width = "88%";
    }
}

resizeTables();
</script>